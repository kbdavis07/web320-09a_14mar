﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PersonalWebSite.Startup))]
namespace PersonalWebSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
